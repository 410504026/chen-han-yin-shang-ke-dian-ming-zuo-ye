﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.ViewModel;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (ModelState.IsValid)
            {

                float m_h = data.Height / 100;
                float bmi = data.Weight / (m_h * m_h);

                string level = "";
                if (bmi < 18.5)
                {
                    level = "太瘦了，給我多吃點!";
                }
                else if (bmi > 18.5 && bmi <= 24)
                {
                    level = "剛剛好，繼續保持!";
                }
                else if (bmi > 24)
                {
                    level = "超重了喇!!!";
                }

                data.BMI = bmi;
                data.Level = level;
            }
            return View(data);
        }
    }
}